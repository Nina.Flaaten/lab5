package labyrinth;

import java.util.Random;

import datastructure.IGrid;
import datastructure.Grid;
import datastructure.Location;


	// 5.1.7) Gå nå gjennom Labyrint-klassene (hovedsaklig Labyrinth og Labyrinth-helper) 
	// og la de bruke Grid og IGrid i stedet for LabyrinthTileGrid.
	// 5.1.8) LabyrinthTileGrid og ILabyrinthTileGrid er nå overflødig. Kjenn på den gode følelsen av 
	// å slette disse filene. Rett opp i eventuelle feil og sjekk at begge labyrint-programmene fungerer.
	// Feilene jeg retter opp er at  LabyrinthTile må stå inni IGrid<>, jeg satte det på Grid også



public class LabyrinthHelper {

	public static IGrid<LabyrinthTile> loadGrid(char[][] source) {
		int rows = source.length;
		int cols = source[0].length;

		IGrid<LabyrinthTile> grid = new Grid<LabyrinthTile>(rows, cols, LabyrinthTile.OPEN);
		for(int col = 0; col < cols; col++) {
			for(int row = 0; row < rows; row++) {
				Location loc = new Location(row, col);
				char symbol = source[row][col];
				LabyrinthTile tile = LabyrinthTile.fromSymbol(symbol);

				if(tile == null) {
					throw new LabyrinthParseException("Incorrect symbol '" + symbol + "' in labyrinth at row " + row + ", col " + col);
				}
				grid.set(loc, tile);
			}
		}

		return grid;
	}

	public static IGrid<LabyrinthTile> makeRandomGrid(int width, int height) {
		Random random = new Random();

		IGrid<LabyrinthTile> grid = new Grid<LabyrinthTile>(width, height, LabyrinthTile.OPEN);
		for(Location loc : grid.locations()) {
			int r = random.nextInt(20);
			LabyrinthTile tile;
			if(r < 15) {
				tile = LabyrinthTile.OPEN;
			} else if(r < 19) {
				tile = LabyrinthTile.WALL;
			} else {
				tile = LabyrinthTile.GOLD;
			}
			grid.set(loc, tile);
		}

		grid.set(new Location(random.nextInt(width), random.nextInt(height)), LabyrinthTile.PLAYER);

		return grid;
	}
}
