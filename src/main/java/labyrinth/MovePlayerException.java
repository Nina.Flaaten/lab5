package labyrinth;

// MovePlayerException er en klasse vi lager som extender Exception-pakken
// MovePlayerException skal returnere en melding til spilleren
// Vi bruker exceptionenen når spilleren prøver å bevege seg et sted hvor den ikke kan bevege seg

public class MovePlayerException extends Exception {
	private static final long serialVersionUID = -6244594256969039528L;

	// Constructor
	public MovePlayerException(String message) {
		super(message);
	}
}
