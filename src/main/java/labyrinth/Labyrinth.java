package labyrinth;

import java.awt.Color;

import datastructure.GridDirection;
import datastructure.IGrid;
import datastructure.Location;


public class Labyrinth implements ILabyrinth {
	private final IGrid<LabyrinthTile> tiles;
	//private int playerX = -1;
	//private int playerY = -1;

	private Location playerLoc;
	private int count;  // 5.3.2 Utvid labyrint-koden, oppretter variabel for count
	boolean playerSet;
	

	// 5.1.7) Gå nå gjennom Labyrint-klassene (hovedsaklig Labyrinth og Labyrinth-helper) 
	// og la de bruke Grid og IGrid i stedet for LabyrinthTileGrid.

	public Location getPlayerLocation() {
		return playerLoc;
	}
	public Labyrinth(IGrid<LabyrinthTile> tiles) throws LabyrinthParseException {
		if(tiles == null) {
			throw new IllegalArgumentException();
		}

		this.tiles = tiles;
		this.count = 0; // 5.3.2 kaller på count i konstruktøren

		int numPlayers = 0;
		for(Location loc : tiles.locations()) {
			if(tiles.get(loc) == LabyrinthTile.PLAYER) {
				numPlayers++;
				playerLoc = loc;
				playerSet = true;
			}
		}
		if(numPlayers != 1) {
			throw new LabyrinthParseException("Labyrinth created with " + numPlayers + " number of players!");
		}

		checkState(this);
	}

	public static void checkState(Labyrinth labyrinth) {
		boolean ok = !labyrinth.playerSet || labyrinth.isValidPos(labyrinth.playerLoc);
		int numPlayers = 0;
		for(Location loc : labyrinth.tiles.locations()) {
			if(labyrinth.tiles.get(loc) == LabyrinthTile.PLAYER) {
				numPlayers++;
			}
		}
		if(labyrinth.playerSet) {
			ok &= numPlayers == 1;
		} else {
			ok &= numPlayers == 0;
		}
		if(!ok) {
			throw new IllegalStateException("bad object");
		}
	}

	@Override
	public LabyrinthTile getCell(Location loc) {
		checkPosition(loc);

		return tiles.get(loc);
	}

	@Override
	public Color getColor(Location loc) {
		if(!isValidPos(loc)) {
			throw new IllegalArgumentException("Location invalid");
		}

		return tiles.get(loc).getColor();
	}

	@Override
	public int numberOfRows() {
		return tiles.numRows();
	}

	/*
	* Returns current amount of player gold
	* 5.3.2 Utvid labyrint-koden
	*
	* bruke getColor? eller bare color? 
	*
	* Prøver å få brukt getColor fra LabyrintTile.java, men vet ikke hvordan. 
	* Aner ikke om dette fungerer dersom jeg får gjort det^
	*  
	*/
	
	/* 5.3.2 Utvid labyrint-koden
	* Metoden updatePlayerCount registrer hvor det er gull og oppdaterer counten
	*
	* Lager en ny metode updateGoldCount, bruker parameteren GridDirection d
	* Ser i movePlayer metoden at en ny lokasjon lages slik:
	* Location newLoc = playerLoc.getNeighbor(d); bruker dette i metoden vår, men nytt navn på newLoc
	* Jeg ser i LabyrinthTile at når en tile har symbolet "g" er det en coin der, jeg vil bruke dette
	* for å se når symbolet equals "g"
	* For å gjøre dette må jeg kalle på metoden getSymbol som bruker parameteren loc
	* Jeg setter den .equals("g")
	* ^ dette setter jeg i en if-setning, if det skjer legg til en i counten
	* getPlayerGold metoden returnerer counten og blir videre brukt når det skal printes ut
	* i spillpanelet
	*/ 
	@Override
	public void updateGoldCount(GridDirection d) {
		Location loc = playerLoc.getNeighbor(d);
		if(getSymbol(loc).equals("g"))
		{
			count++;
		} 
	} 

	// 5.3.2 Utvid labyrint-koden
	// denne metoden returnerer counten, counten oppdateres i updateGold
	@Override
	public int getPlayerGold() {
		return count;
	}

	@Override
	public int getPlayerHitPoints() {
		return 0;
	}

	@Override
	public int numberOfColumns() {
		return tiles.numColumns();
	}

	@Override
	public boolean isPlaying() {
		return playerSet;
	}

	private boolean isValidPos(Location loc) {
		return loc.row >= 0 && loc.row < tiles.numRows() //
				&& loc.col >= 0 && loc.col < tiles.numColumns();
	}

	private void checkPosition(Location loc) {
		if(!isValidPos(loc)) {
			throw new IndexOutOfBoundsException("Row and column indices must be within bounds");
		}
	}

	/**
	 * This method checks if a player can move to a given location
	 * A player can not go to the location if there is a wall or
	 * if the location is outside the bounds of the grid.
	 * 
	 * @param d
	 * @throws MovePlayerException
	 * 
	 * 5.2.2) Skriv forkrav til movePlayer-metoden. 
	 * Forkrav-sjekker består typisk av en if-setning som sjekker egenskaper ved argument-verdiene. 
	 * 
	 * forkrav til movePlayer-metoden:
	 * if(<argument-egenskap>){
	 *    <feilhåndtering>
	 * }
	 * ...
	 * //resten av metoden
	 * 
	 * playerCanGo sjekker om et flytt er gyldig
	 * jeg bruker den som hjelpemetode til å sjekke argumentet mitt
	 * 
	 * Override forteller oss at movePlayer i Labyrinth.java er en implementasjon av movePlayer
	 * i interfacet Ilabyrinth.java. Det vil si at selv om movePlayer kalles på en variabel som
	 * har deklarert type ILabyrinth, så er den konkrete metoden som blir kalt under kjøring i Labyrinth.
	 */
	@Override
	public void movePlayer(GridDirection d) throws MovePlayerException {
		if (!playerCanGo(d)) {
			throw new MovePlayerException("Out of bounds.");
		}
		Location newLoc = playerLoc.getNeighbor(d);
		tiles.set(playerLoc, LabyrinthTile.OPEN);
		playerLoc = newLoc;
		tiles.set(newLoc, LabyrinthTile.PLAYER);
		checkState(this);
	}



	
	@Override
	public boolean playerCanGo(GridDirection d) {
		if(d == null) {
			throw new IllegalArgumentException();
		}

		return playerCanGoTo(playerLoc.getNeighbor(d));
	}

	/**
	 * This method checks if a player can move to a given location
	 * A player can not go to the location if there is a wall or
	 * if the location is outside the bounds of the grid.
	 * 
	 * @param loc
	 * @return
	 */
	private boolean playerCanGoTo(Location loc) {
		if(!isValidPos(loc)) {
			return false;
		}

		return tiles.get(loc) != (LabyrinthTile.WALL);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		for(int y = tiles.numColumns() - 1; y >= 0; y--) {
			for(int x = 0; x < tiles.numRows(); x++) {
				sb.append(getSymbol(new Location(x, y)));
			}
			sb.append("\n");
		}
		return sb.toString();
	}

	/**
	 * No bounds checking will be done for the given {@code loc}.
	 */
	private String getSymbol(Location loc) {
		return String.valueOf(tiles.get(loc).getSymbol());
	}

	@Override
	public Iterable<Location> locations() {
		return tiles.locations();
	}
}
